<?php

/**
 * @file
 * Core functionality for search exclude node.
 */

define('SEARCH_EXCLUDE_NODE', 'search_exlude_node');

/**
 * Implements hook_permission().
 */
function search_exclude_node_permission() {
  return array(
    'administer exclude from core search' => array(
      'title' => t('Administer exclude from core search'),
      'description' => t('Allows users the functionality to remove a node from core search.'),
    ),
  );
}

/**
 * Implements hook_node_load().
 */
function search_exclude_node_node_load($nodes, $types) {
  $excluded_nids = variable_get(SEARCH_EXCLUDE_NODE, array());

  // Check if the node id exists in the list and asign accordingly.
  foreach ($nodes as $node) {
    if (in_array($node->nid, $excluded_nids)) {
      $nodes[$node->nid]->search_exclude_node = TRUE;
    }
    else {
      $nodes[$node->nid]->search_exclude_node = FALSE;
    }
  }
}

/**
 * Implements hook_node_delete().
 */
function search_exclude_node_node_delete($node) {
  $excluded_nids = variable_get(SEARCH_EXCLUDE_NODE, array());
  unset($excluded_nids[$node->nid]);
  variable_set(SEARCH_EXCLUDE_NODE, $excluded_nids);
}

/**
 * Implements hook_form_alter().
 */
function search_exclude_node_form_node_form_alter(&$form, &$form_state, $form_id) {
  if (user_access('administer exclude from search')) {
    $form['search_exclude_node'] = array(
      '#type' => 'fieldset',
      '#title' => 'Exclude from core search',
      '#group' => 'additional_settings',
    );
    $form['search_exclude_node']['search_exclude_node'] = array(
      '#type' => 'checkbox',
      '#title' => t('Exclude this node from core search.'),
      '#required' => FALSE,
      '#default_value' => isset($form_state['node']->search_exclude_node) ? $form_state['node']->search_exclude_node : '',
      '#description' => t('This will ensure that this node is excluded from core search results.'),
      '#weight' => -1,
    );
    $form['#submit'][] = 'search_exclude_node_form_submit';
  }
}

/**
 * Submit handler to save the nid to the list if this node is to be excluded.
 */
function search_exclude_node_form_submit($form, &$form_state) {
  $excluded_nids = variable_get(SEARCH_EXCLUDE_NODE, array());
  if (!empty($form_state['values']['search_exclude_node'])) {
    $excluded_nids[$form_state['values']['nid']] = $form_state['values']['nid'];
  }
  else {
    unset($excluded_nids[$form_state['values']['nid']]);
  }
  variable_set(SEARCH_EXCLUDE_NODE, $excluded_nids);
}

/**
 * Overrides template_preprocess_search_results().
 */
function search_exclude_node_preprocess_search_results(&$variables) {
  $excluded_nids = variable_get(SEARCH_EXCLUDE_NODE, array());

  $variables['search_results'] = '';
  if (!empty($variables['module'])) {
    $variables['module'] = check_plain($variables['module']);
  }
  foreach ($variables['results'] as $result) {
    if (!in_array($result['node']->nid, $excluded_nids)) {
      $variables['search_results'] .= theme('search_result', array('result' => $result, 'module' => $variables['module']));
    }
  }
  $variables['pager'] = theme('pager', array('tags' => NULL));
  $variables['theme_hook_suggestions'][] = 'search_results__' . $variables['module'];
}
